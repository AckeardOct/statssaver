#include "tableunit.h"

TableUnit::TableUnit(QString eventName, QVector<EventUnitData*> * unitData, QWidget *parent) :
    QWidget(parent)
{
    // ������� ����� � ����� ������
    QFrame * upLine = new QFrame;
    upLine->setFrameStyle(QFrame::HLine);

    QHBoxLayout * inHLay = new QHBoxLayout;

    QVBoxLayout * genLayout = new QVBoxLayout(this);    
    genLayout->addWidget(upLine);
    genLayout->addLayout(inHLay);

    // � ������ ������
    eventLbl = new QLabel(eventName);
    eventLbl->setAlignment(Qt::AlignCenter);
    cntLbl = new QLabel("0");
    cntLbl->setAlignment(Qt::AlignRight);
    QVBoxLayout * eventLay = new QVBoxLayout;
    eventLay->addWidget(eventLbl);
    eventLay->addWidget(cntLbl);

    for(int i = 0; i < unitData->size(); i++)
    {
        evUnit.append(new EventUnit);
        evUnit.at(i)->setHeader(unitData->at(i)->header);
        evUnit.at(i)->setValue(unitData->at(i)->value);
    }

    inHLay->addLayout(eventLay);
    for(int i = 0; i < evUnit.size(); i++)
        inHLay->addWidget(evUnit.at(i));

    reCount();

    for(int i = 0; i < evUnit.size(); i++)
    {
        connect(evUnit.at(i),SIGNAL(valueChanged()),SLOT(reCount()));
        connect(evUnit.at(i),SIGNAL(signalMinus()),SLOT(slotMinus()));
        connect(evUnit.at(i), SIGNAL(signalPlus()),SLOT(slotPlus()));
    }


    this->setLayout(genLayout);
}

QString TableUnit::getHeader()
{
    return eventLbl->text();
}

void TableUnit::reCount()
{
    uint tmp = 0;
    for(int i = 0; i < evUnit.size(); i++)
        tmp += evUnit.at(i)->getValue();
    cntLbl->setText(QString::number(tmp));
}

void TableUnit::slotMinus()
{
    if(cntLbl->text().toUInt() > 0)
        cntLbl->setText(QString::number(cntLbl->text().toUInt() - 1));
}

void TableUnit::slotPlus()
{
    cntLbl->setText(QString::number(cntLbl->text().toUInt() + 1));
}

EventUnit::EventUnit()
{
    header = "EventValue";
    value = 0;

    QFrame * hLine = new QFrame;
    hLine->setFrameStyle(QFrame::VLine);
    QVBoxLayout * vLay = new QVBoxLayout;

    QHBoxLayout * genLay = new QHBoxLayout(this);
    genLay->addWidget(hLine);
    genLay->addLayout(vLay);

    // ����
    headerLbl = new QLabel(header);
    headerLbl->setAlignment(Qt::AlignCenter);

    QFrame * upLine = new QFrame;
    upLine->setFrameStyle(QFrame::HLine);

    QHBoxLayout * downLay = new QHBoxLayout;

    vLay->addWidget(headerLbl);
    vLay->addWidget(upLine);
    vLay->addLayout(downLay);

    // ���
    QPushButton * minBtn = new QPushButton("-");
    minBtn->setMaximumWidth(20);
    numLbl = new QLabel(QString::number(value));
    numLbl->setAlignment(Qt::AlignCenter);
    QPushButton * plusBtn = new QPushButton("+");
    plusBtn->setMaximumWidth(20);

    downLay->addWidget(minBtn);
    downLay->addWidget(numLbl);
    downLay->addWidget(plusBtn);

    connect(minBtn,SIGNAL(clicked()),SLOT(slotMinus()));
    connect(plusBtn,SIGNAL(clicked()),SLOT(slotPlus()));
}

void EventUnit::setHeader(QString str)
{
    headerLbl->setText(str);
}

void EventUnit::setValue(uint val)
{
    numLbl->setText(QString::number(val));
    emit valueChanged();
}

unsigned int EventUnit::getValue()
{
    return numLbl->text().toUInt();
}

QString EventUnit::getHeader()
{
    return headerLbl->text();
}

void EventUnit::slotMinus()
{
    if(numLbl->text().toUInt() > 0)
    {
        numLbl->setText(QString::number(numLbl->text().toUInt() - 1));
        emit signalMinus();
    }
}

void EventUnit::slotPlus()
{
    numLbl->setText(QString::number(numLbl->text().toUInt() + 1));
    emit signalPlus();
}

EventUnitData::EventUnitData(QString head, uint val) :
    header(head)
  , value(val)
{
}
