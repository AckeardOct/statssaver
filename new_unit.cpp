#include "new_unit.h"

NewUnit::NewUnit(QWidget *parent) : QWidget(parent)
{
    nameLine = new QLineEdit("Name");
    nameLine->selectAll();
    QPushButton * nextBtn = new QPushButton("Next");

    //Layout Setup
    QVBoxLayout * vbx = new QVBoxLayout(this);
    vbx->addWidget(nameLine);
    vbx->addWidget(nextBtn);

    setLayout(vbx);

    //connect
    connect(nextBtn, SIGNAL(clicked()), SLOT(slotNext()));
    connect(&newEvent, SIGNAL(signalNext(QString,uint)), SLOT(slotAddEvent(QString,uint)));
    connect(&newEvent, SIGNAL(signalComplete()), SLOT(slotComplete()));
}

void NewUnit::slotNext()
{
    unitData.header = nameLine->text();
    this->close();
    newEvent.show();
}

void NewUnit::slotAddEvent(QString header, unsigned int value)
{
    unitData.data.append(new EventUnitData(header, value));
}

void NewUnit::slotComplete()
{
    // �������� ����� ���������� � xml � �����
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file("data.xml");
    pugi::xml_node node = doc.child("root").child("tableUnits").append_child("tableUnit");
    node.append_attribute("name").set_value(unitData.header.toStdString().c_str());

    node = node.append_child("eventDatas");
    for(int i = 0; i < unitData.data.size(); i++)
    {
        node = node.append_child("eventData");
        node.append_attribute("name").set_value(unitData.data.at(i)->header.toStdString().c_str());
        node.append_attribute("value").set_value(unitData.data.at(i)->value);
        node = node.parent();
    }

    doc.save_file("data.xml");

    emit signalData();
    this->close();
}

NewEvent::NewEvent(QWidget *parent)
{
    eventNameLine = new QLineEdit("Event");
    eventNameLine->selectAll();
    QLabel * valueLbl = new QLabel("Value:");
    valueBox = new QSpinBox;
    valueBox->setValue(0);
    QPushButton * nextBtn = new QPushButton("Next");
    QPushButton * completeBtn = new QPushButton("Complete");

    //Layout Setup
    QHBoxLayout * hbx = new QHBoxLayout;
    hbx->addWidget(nextBtn);
    hbx->addWidget(completeBtn);

    QHBoxLayout * valueHbx = new QHBoxLayout;
    valueHbx->addWidget(valueLbl);
    valueHbx->addWidget(valueBox);

    QVBoxLayout * vbx = new QVBoxLayout(this);
    vbx->addWidget(eventNameLine);
    vbx->addLayout(valueHbx);
    vbx->addLayout(hbx);

    setLayout(vbx);

    //connect
    connect(completeBtn, SIGNAL(clicked()), SLOT(slotComplete()));
    connect(nextBtn, SIGNAL(clicked()), SLOT(slotNext()));
}

void NewEvent::slotNext()
{
    emit signalNext(eventNameLine->text(), valueBox->value());
    eventNameLine->setText("Name");
    valueBox->setValue(0);
    eventNameLine->setFocus();
    eventNameLine->selectAll();
}

void NewEvent::slotComplete()
{
    emit signalComplete();
    this->close();
}
