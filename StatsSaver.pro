#-------------------------------------------------
#
# Project created by QtCreator 2014-12-07T18:45:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StatsSaver
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tableunit.cpp \
    pugi/pugixml.cpp \
    new_unit.cpp

HEADERS  += mainwindow.h \
    tableunit.h \
    pugi/pugiconfig.hpp \
    pugi/pugixml.hpp \
    new_unit.h

FORMS    += mainwindow.ui
