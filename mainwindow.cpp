#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    newUnit(new NewUnit)
    //ui(new Ui::MainWindow)
{
    //ui->setupUi(this);       

    QMenuBar * menuBar = new QMenuBar;
    QMenu * fileMenu = new QMenu("File");
    fileMenu->addAction("New Unit", newUnit, SLOT(show()), QKeySequence("CTRL+N"));
    fileMenu->addAction("Save", &widget, SLOT(saveData()), QKeySequence("CTRL+S"));
    menuBar->addMenu(fileMenu);
    menuBar->addAction("Save", &widget, SLOT(saveData()));

    resize(50, 50);
    setCentralWidget(&widget);
    setMenuBar(menuBar);

    connect(newUnit, SIGNAL(signalData()), &widget, SLOT(upData()));
}

MainWindow::~MainWindow()
{
    //delete ui;
}

bool CWidget::getData()
{
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file("data.xml");

    pugi::xml_node tableUnits = doc.child("root").child("tableUnits");

    for(pugi::xml_node tableUnit = tableUnits.first_child(); tableUnit;
        tableUnit = tableUnit.next_sibling())
    {
        QString header = tableUnit.attribute("name").as_string();
        QVector<EventUnitData*> evData;

        pugi::xml_node eventDatas = tableUnit.child("eventDatas");

        for(pugi::xml_node eventData = eventDatas.first_child(); eventData;
            eventData = eventData.next_sibling())
        {
            evData.append(new EventUnitData);

            for(pugi::xml_attribute attr = eventData.first_attribute(); attr;
                attr = attr.next_attribute())
            {
                if(QString (attr.name()) == "name")
                    evData.last()->header = attr.as_string();
                else if(QString (attr.name()) == "value")
                    evData.last()->value = attr.as_uint();
            }
        }

        genData.append(new TableUnitData);
        genData.last()->header = header;
        genData.last()->data = evData;
    }

    return true;
}

bool CWidget::saveData()
{
    for(int i = 0; i < tblUnits.size(); i++)
    {
        genData.at(i)->header = tblUnits.at(i)->getHeader();

        for(int j = 0; j < tblUnits.at(i)->evUnit.size(); j++)
        {
            genData.at(i)->data.at(j)->header = tblUnits.at(i)->evUnit.at(j)->getHeader();
            genData.at(i)->data.at(j)->value = tblUnits.at(i)->evUnit.at(j)->getValue();
        }
    }

    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file("data.xml");

    pugi::xml_node tableUnits = doc.child("root").child("tableUnits");

    int i = 0;
    for(pugi::xml_node tableUnit = tableUnits.first_child(); tableUnit;
        tableUnit = tableUnit.next_sibling())
    {
        tableUnit.attribute("name").set_value(genData.at(i)->header.toStdString().c_str());
        pugi::xml_node eventDatas = tableUnit.child("eventDatas");

        int j = 0;
        for(pugi::xml_node eventData = eventDatas.first_child(); eventData;
            eventData = eventData.next_sibling())
        {
            for(pugi::xml_attribute attr = eventData.first_attribute(); attr;
                attr = attr.next_attribute())
            {
                if(QString (attr.name()) == "name")
                    attr.set_value(genData.at(i)->data.at(j)->header.toStdString().c_str());
                if(QString (attr.name()) == "value")
                    attr.set_value(genData.at(i)->data.at(j)->value);
                    //qDebug() << genData.at(i)->data.at(j)->value;
            }
            j++;
        }
        i++;
    }

    doc.save_file("data.xml");    

    return true;
}


CWidget::CWidget()
{
    this->getData();

    vbx = new QVBoxLayout(this);

    for(int i = 0; i < genData.size(); i++)
    {
        tblUnits.append(new TableUnit(genData.at(i)->header, &genData.at(i)->data));
        vbx->addWidget(tblUnits.last());
    }

    this->setLayout(vbx);
}

void CWidget::upData()
{

}
