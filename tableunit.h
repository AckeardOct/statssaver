#ifndef TABLEUNIT_H
#define TABLEUNIT_H

#include <QWidget>
#include <QLabel>
#include <QLine>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFrame>
#include <QPushButton>
#include <QVector>
#include <QDebug>
#include <QApplication>

class EventUnitData
{
public:
    QString header;
    unsigned int value;
    EventUnitData() {};
    EventUnitData(QString head, uint val = 0);
    ~EventUnitData() {};
};

struct TableUnitData
{
    QString header;
    QVector<EventUnitData*> data;
};

class EventUnit : public QWidget
{
    Q_OBJECT
private:
    QLabel * numLbl;
    QLabel * headerLbl;
    QString header;
    uint value;
    EventUnitData evUData;
public:
    EventUnit();
    ~EventUnit() {};
    void setHeader(QString str);
    void setValue(uint val);
    unsigned int getValue();
    QString getHeader();
public slots:
    void slotMinus();
    void slotPlus();
signals:
    void signalMinus();
    void signalPlus();
    void valueChanged();
};

class TableUnit : public QWidget
{
    Q_OBJECT
private:
    QLabel * eventLbl;    
    QLabel * cntLbl;
    TableUnitData tuData;
public:
    QVector<EventUnit*> evUnit;
    TableUnit(QString eventName ,QVector<EventUnitData*> * unitData ,QWidget *parent = 0);
    ~TableUnit() {};
    QString getHeader();
public slots:
    void reCount();
    void slotMinus();
    void slotPlus();
};

#endif // TABLEUNIT_H
