#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "tableunit.h"
#include "new_unit.h"
#include <QScrollArea>
#include <QVBoxLayout>
#include <QPushButton>
#include <QVector>
#include "pugi/pugixml.hpp"
#include <QDebug>
#include <QMenu>
#include <QMenuBar>
#include <QAction>

//namespace Ui {
//class MainWindow;
//}

class CWidget : public QWidget
{
    Q_OBJECT
private:
    QVector<TableUnitData*> genData;
    QVector<TableUnit *> tblUnits;
    QVBoxLayout * vbx;
public:
    CWidget();
    ~CWidget() { saveData(); };
    bool getData();
public slots:
    bool saveData();
    void upData();
};

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    CWidget widget;
    NewUnit * newUnit;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    //Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
