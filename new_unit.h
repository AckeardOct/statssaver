#ifndef NEWUNIT_H
#define NEWUNIT_H

#include "tableunit.h"
#include "pugi/pugixml.hpp"
#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSpinBox>
#include <QLabel>
#include <QDebug>

class NewEvent  : public QWidget
{
    Q_OBJECT
private:
    QLineEdit * eventNameLine;
    QSpinBox * valueBox;
public:
    NewEvent(QWidget * parent = 0);
public slots:
    void slotNext();
    void slotComplete();
signals:
    void signalNext(QString name, unsigned int val);
    void signalComplete();
};

class NewUnit : public QWidget
{
    Q_OBJECT
private:
    QLineEdit * nameLine;
    NewEvent newEvent;
    TableUnitData unitData;
public:
    explicit NewUnit(QWidget *parent = 0);
    ~NewUnit() { };
public slots:
    void slotNext();
    void slotAddEvent(QString header, unsigned int value);
    void slotComplete();    
signals:
    void signalData();
};

#endif // NEWUNIT_H
